##Task:
For the coding project, we ask folks to write a tic-tac-toe game to allows us to play against a computer. It doesn't have to be perfect, but we expect not to win (so the computer always wins or ties). The player should be able to make the first move. Ideally we get a link to play and a way to see the source code when it's working. It can be in any language and there is no timeframe, since we understand this requires time that you have to fit in.

##Assumptions and Description:
Standard rules. Board size is 3x3.
Bot can't lose the game. If you find the way without hacking the code, I believe that universe will crush at exact this time.
I'm using Minimax algorithm, and count only +1/-1 if it leads to victory or defeat, if victory movement has been found I don't evaluate any more. Because we have only 9 cells, I assume that it's ok to evaluate the whole tree of possible moves. If board size would be bigger, maybe I should think more about ranking of each turn (machile learning could be helpful here), but for 3x3 I decided to process whole tree.
I'm using javascript to not bother you with any limitations like .net framework, jdk or so on, but in real life I would better use something like C# or Java to hide all states and show only methods that implements specific interface.
Also, I don't include tests or min versions of js and css files, because it won't affect the solution but will consume some time. If you want, I can do it. Same for typescript

##How to use localy:
launch index.html and play

##Online demo:
[https://debuggio.bitbucket.io/](https://debuggio.bitbucket.io/)

If you have any questions feel free to ask