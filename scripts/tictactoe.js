/* Implements main tictactoe game entry */
require(["model", "board", "ai"], function (model, board, ai) {
    var curPlayer = model.constants.X;

    /* Disblays message to user */
    var displayMessage = function (message) {
        if(message === null) {
            throw new Error('argument "message" is required!');
        } 

        $('.message').html(message);
    };

    /* Switches players */
    var switchPlayer = function () {
        curPlayer = curPlayer === model.constants.X ? model.constants.O : model.constants.X;
        displayMessage('Current Player: ' + curPlayer);
    };

    /* Holds logic to make move */
    var makePlayerMove = function ($square, gameSquare) {
        board.makeMove(gameSquare, curPlayer);
        $square.html(curPlayer);
        $square.removeClass('square-hover');
    };

    /* End game logic */
    var endGame = function () {
        var endMessage;
        if (board.hasWon(model.constants.X)) {
            endMessage = "Game Over. Universe will crush in 5...4...3...2...1...... because this bot can't loose";
            highlightWonCombination();
        } else if (board.hasWon(model.constants.O)) {
            endMessage = "Game Over. Sorry, you have lost... again";
            highlightWonCombination();
        } else {
            endMessage = "Game over. It's a draw. Keep trying! You can win!";
        }

        $('.message').addClass('end-message');
        displayMessage(endMessage);

        $('.gameboard').off('click');
        $('.play-again').show().on('click', function () {
            restartgame();
        });
    };

    /** If game changes to something more complex - change this method */
    var restartgame = function() {
        location.reload();
    };

    /* Highlights victory combination on UI */
    var highlightWonCombination = function () {
        var wonCombination = board.getWonCombination();
        for (var i = 0; i < wonCombination.length; i++) {
            $('.square[data-row="' + wonCombination[i][0] + '"][data-column="' + wonCombination[i][1] + '"]').addClass('won-square');
        }
    };

    /* Implements logic to make user turn */
    var processPlayerTurn = function ($square) {
        if($square === null) {
            throw new Error('argument "$square" is required!');
        }
        
        var column = $square.attr('data-column');
        var row = $square.attr('data-row');
        var gameSquare = new model.gameSquare(row, column);

        if (board.isValidMove(gameSquare)) {
            makePlayerMove($square, gameSquare);
            if (board.isGameOver()) {
                endGame();
            } else {
                switchPlayer();
                processAiTurn();
            }
        }
    };

    /* Implements logic to make AI turn */
    var processAiTurn = function () {
        var nextTurn = ai.getNextTurn(board);
        var square = $('.square[data-row="' + nextTurn.row + '"][data-column="' + nextTurn.column + '"]');
        makePlayerMove(square, nextTurn);
        if (board.isGameOver()) {
            endGame();
        } else {
            switchPlayer();
        }
    };

    /* Adds event listener for user to make turns */
    $(document).ready(function () {
        $('.gameboard').on('click', '.square', function () {
            processPlayerTurn($(this));
        });
    });
})();
