/* Model classes and constants */
define(function () {
    /* Constants */
    var constants = {
        X: 'X',
        O:'O',
        Empty: ' '
    };

    /* Holds Row and Column index */
    var gamesquare = function (row, column) {
        if(row === null) {
            throw new Error('row is required!');
        }
        if(column === null) {
            throw new Error('column is required!');
        }
                        
        this.row = row;
        this.column = column;
    };

    return {
        constants: constants,
        gameSquare: gamesquare
    };
});