/* AI logic for tic-tac-tor */
define(['model'], function (model) {
    var ai = function(model) {
        /* Holds result for next turn evaluation */
        var nextTurn = {};
        /* Recursivly calculates score for next possible turns and stores result in nextTurn */
        var processNextTurn = function(board, depth, isAiTurn) {
            if (board.hasWon(model.constants.X))  {
                return -1;
            }
            if (board.hasWon(model.constants.O)) {
                return +1;
            }

            var availableGameSquares = board.getAvailableGameSquares();
            if (availableGameSquares.length === 0) {
                return 0;
            }

            var min = Number.POSITIVE_INFINITY,
                max = Number.NEGATIVE_INFINITY;

            for (var i = 0; i < availableGameSquares.length; i++) {
                var gameSquare = availableGameSquares[i];
                var currentScore;
                if (isAiTurn) {
                    board.makeMove(gameSquare, model.constants.O);
                    currentScore = processNextTurn(board, depth + 1, false);
                    max = Math.max(currentScore, max);

                    if (currentScore >= 0) {
                        if (depth === 0) {
                            nextTurn = gameSquare;
                        }
                    }
                    if (currentScore === 1) {
                        board.boardState[gameSquare.row][gameSquare.column] = model.constants.Empty;
                        break;
                    }
                    if (i === availableGameSquares.length - 1 && max < 0) {
                        if (depth === 0) {
                            nextTurn = gameSquare;
                        }
                    }
                } else {
                    board.makeMove(gameSquare, model.constants.X);
                    currentScore = processNextTurn(board, depth + 1, true);
                    min = Math.min(currentScore, min);
                    if (min === -1) {
                        board.boardState[gameSquare.row][gameSquare.column] = model.constants.Empty;
                        break;
                    }
                }
                board.boardState[gameSquare.row][gameSquare.column] = model.constants.Empty;
            }
            return isAiTurn ? max : min;
        };

        /** Processes next turn for ai 
         * @returns {gameSquare} for next ai turn
        */
        this.getNextTurn = function(board) {
            processNextTurn(board, 0, true);
            return nextTurn;
        };
    };
    
    return new ai(model);     
});
