/* Implements methods for game board manipulation */
define(['model'], function (model) {
    var board = function() {
        /** Current state of the board */
        this.boardState = [[model.constants.Empty, model.constants.Empty, model.constants.Empty],
                            [model.constants.Empty, model.constants.Empty, model.constants.Empty],
                            [model.constants.Empty, model.constants.Empty, model.constants.Empty]];

        /** Checks if player could perform such move */
        this.isValidMove = function (gameSquare) {
            if (this.boardState[gameSquare.row][gameSquare.column] === model.constants.Empty) {
                return true;
            } else {
                return false;
            }
        };
        /** Changes state of the board */
        this.makeMove = function (gameSquare, player) {
            this.boardState[gameSquare.row][gameSquare.column] = player;
        };

        /** Checks if one of players has won the game
         * @returns {Boolean} Returns true if specified player has won the game.
        */
        this.hasWon = function (player) {
            if(player === null) {
                throw new Exception("argument player is required");
            }

            if ((this.boardState[0][0] === this.boardState[1][1] && this.boardState[0][0] === this.boardState[2][2] && this.boardState[0][0] === player) || (this.boardState[0][2] === this.boardState[1][1] && this.boardState[0][2] === this.boardState[2][0] && this.boardState[0][2] === player)) {
                return true;
            }
            for (var i = 0; i < this.boardState.length; i++) {
                if (((this.boardState[i][0] === this.boardState[i][1] && this.boardState[i][0] === this.boardState[i][2] && this.boardState[i][0] === player) || (this.boardState[0][i] === this.boardState[1][i] && this.boardState[0][i] === this.boardState[2][i] && this.boardState[0][i] === player))) {
                    return true;
                }
            }
            return false;
        };
        
        /** Returns all available squares for move */    
        this.getAvailableGameSquares = function () {
            var availablePoints = [];
            for (var i = 0; i < this.boardState.length; i++) {
                for (var j = 0; j < this.boardState.length; j++) {
                    if (this.boardState[i][j] === model.constants.Empty) {
                        availablePoints.push(new model.gameSquare(i, j));
                    }
                }
            }
            return availablePoints;
        };
        /** Checks if game is over */  
        this.isGameOver = function () {
            return (this.hasWon(model.constants.X) || this.hasWon(model.constants.O) || this.getAvailableGameSquares().length === 0);
        };
        /** Returns won combination of game squares
         * @returns {Array} of gameSquares if any player has won the game, otherwise null
         * */          
        this.getWonCombination = function () {
            /** All possible win combinations all squares in the same row + all squares in the same column + 2 diagonals*/
            var winCombinations = [
                [[0, 0], [0, 1], [0, 2]], [[1, 0], [1, 1], [1, 2]], [[2, 0], [2, 1], [2, 2]],
                [[0, 0], [1, 0], [2, 0]], [[0, 1], [1, 1], [2, 1]], [[0, 2], [1, 2], [2, 2]],
                [[0, 0], [1, 1], [2, 2]], [[0, 2], [1, 1], [2, 0]]
            ];

            for (var i = 0; i < winCombinations.length; i++) {
                var winCombination = winCombinations[i];
                if (this.boardState[winCombination[0][0]][winCombination[0][1]] === this.boardState[winCombination[1][0]][winCombination[1][1]] &&
                    this.boardState[winCombination[0][0]][winCombination[0][1]] === this.boardState[winCombination[2][0]][winCombination[2][1]]) {
                    return winCombination;
                }
            }
        };
    };

    return new board();
});